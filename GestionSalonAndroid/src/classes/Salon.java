package classes;

import java.util.Calendar;
import android.graphics.Bitmap;

/**
 * Classe Salon
 * @author ggosseli
 *
 */
public class Salon {
	private int id_salon;
	private String nom;
	private String nom_representant;
	private String prenom_representant;
	private CategorieDuSalon categorie;
	private Calendar date_debut;
	private Calendar date_fin;
	private String rue;
	private int code_postal;
	private String ville;
	private String description;
	private int mise_avant_stand;
	private int mise_avant_evenement;
	private int participation_jeu;
	private Bitmap image;
	private Bitmap icone;
	private TypeDeJeuSalon type_de_jeu;
	private int etat;
	private int version;
	
	/**
	 * Constructeur par défaut
	 */
	public Salon() {
		super();
	}

	/**
	 * Constructeur
	 * @param id
	 * @param nom
	 * @param nom_du_representant
	 * @param prenom_du_representant
	 * @param categorie
	 * @param date_de_debut
	 * @param date_de_fin
	 * @param rue
	 * @param code_postal
	 * @param ville
	 * @param description
	 * @param mise_en_avant_de_stand
	 * @param mise_en_avant_d_evenement
	 * @param participation_au_jeu
	 * @param image
	 * @param icone
	 * @param type_de_jeu
	 * @param etat
	 * @param version
	 */
	public Salon(int id, String nom, String nom_du_representant,
			String prenom_du_representant, CategorieDuSalon categorie,
			Calendar date_de_debut, Calendar date_de_fin,String rue, int code_postal, String ville,
			String description, int mise_en_avant_de_stand,
			int mise_en_avant_d_evenement, int participation_au_jeu,
			Bitmap image, Bitmap icone, TypeDeJeuSalon type_de_jeu,
			int etat, int version) {
		super();
		this.id_salon = id;
		this.nom = nom;
		this.nom_representant = nom_du_representant;
		this.prenom_representant = prenom_du_representant;
		this.categorie = categorie;
		this.date_debut = date_de_debut;
		this.date_fin = date_de_fin;
		this.rue = rue;
		this.code_postal = code_postal;
		this.ville = ville;
		this.description = description;
		this.mise_avant_stand = mise_en_avant_de_stand;
		this.mise_avant_evenement = mise_en_avant_d_evenement;
		this.participation_jeu = participation_au_jeu;
		this.image = image;
		this.icone = icone;
		this.type_de_jeu = type_de_jeu;
		this.etat = etat;
		this.version = version;
	}
	
	/**
	 * @return id_salon
	 */
	public int getId_salon() {
		return id_salon;
	}

	/**
	 * @param id_salon
	 */
	public void setId_salon(int id_salon) {
		this.id_salon = id_salon;
	}

	/**
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return nom_representant
	 */
	public String getNom_representant() {
		return nom_representant;
	}

	/**
	 * @param nom_du_representant
	 */
	public void setNom_representant(String nom_du_representant) {
		this.nom_representant = nom_du_representant;
	}

	/**
	 * @return prenom_representant
	 */
	public String getPrenom_representant() {
		return prenom_representant;
	}

	/**
	 * @param prenom_du_representant
	 */
	public void setPrenom_representant(String prenom_du_representant) {
		this.prenom_representant = prenom_du_representant;
	}

	/**
	 * @return categorie
	 */
	public CategorieDuSalon getCategorie() {
		return categorie;
	}

	/**
	 * @param categorie
	 */
	public void setCategorie(CategorieDuSalon categorie) {
		this.categorie = categorie;
	}

	/**
	 * @return date_debut
	 */
	public Calendar getDate_debut() {
		return date_debut;
	}

	/**
	 * @param date_de_debut
	 */
	public void setDate_debut(Calendar date_de_debut) {
		this.date_debut = date_de_debut;
	}

	/**
	 * @return date_fin
	 */
	public Calendar getDate_fin() {
		return date_fin;
	}

	/**
	 * @param date_de_fin
	 */
	public void setDate_fin(Calendar date_de_fin) {
		this.date_fin = date_de_fin;
	}

	/**
	 * @return rue
	 */
	public String getRue() {
		return rue;
	}

	/**
	 * @param rue
	 */
	public void setRue(String rue) {
		this.rue = rue;
	}

	/**
	 * @return code_postal
	 */
	public int getCode_postal() {
		return code_postal;
	}

	/**
	 * @param code_postal
	 */
	public void setCode_postal(int code_postal) {
		this.code_postal = code_postal;
	}

	/**
	 * @return ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * @param ville
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return mise_avant_stand
	 */
	public int getMise_avant_stand() {
		return mise_avant_stand;
	}

	/**
	 * @param mise_en_avant_de_stand
	 */
	public void setMise_avant_stand(int mise_en_avant_de_stand) {
		this.mise_avant_stand = mise_en_avant_de_stand;
	}

	/**
	 * @return mise_avant_evenement
	 */
	public int getMise_avant_evenement() {
		return mise_avant_evenement;
	}

	/**
	 * @param mise_en_avant_d_evenement
	 */
	public void setMise_avant_evenement(int mise_en_avant_d_evenement) {
		this.mise_avant_evenement = mise_en_avant_d_evenement;
	}

	/**
	 * @return participation_jeu
	 */
	public int getParticipation_au_jeu() {
		return participation_jeu;
	}

	/**
	 * @param participation_au_jeu
	 */
	public void setParticipation_au_jeu(int participation_au_jeu) {
		this.participation_jeu = participation_au_jeu;
	}

	/**
	 * @return image
	 */
	public Bitmap getImage() {
		return image;
	}

	/**
	 * @param image
	 */
	public void setImage(Bitmap image) {
		this.image = image;
	}

	/**
	 * @return icone
	 */
	public Bitmap getIcone() {
		return icone;
	}

	/**
	 * @param icone
	 */
	public void setIcone(Bitmap icone) {
		this.icone = icone;
	}

	/**
	 * @return type_de_jeu
	 */
	public TypeDeJeuSalon getType_de_jeu() {
		return type_de_jeu;
	}

	/**
	 * @param type_de_jeu
	 */
	public void setType_de_jeu(TypeDeJeuSalon type_de_jeu) {
		this.type_de_jeu = type_de_jeu;
	}

	/**
	 * @return etat
	 */
	public int getEtat() {
		return etat;
	}

	/**
	 * @param etat
	 */
	public void setEtat(int etat) {
		this.etat = etat;
	}

	/**
	 * @return version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @param version
	 */
	public void setVersion(int version) {
		this.version = version;
	}
}