package classes;

import android.graphics.Bitmap;

/**
 * Classe Stand
 * @author ggosseli
 *
 */
public class Stand {
	private int id_stand;
	private String numero;
	private String nom;
	private String nom_representant;
	private String prenom_representant;
	private TypeDuStand type;
	private CategorieDuSalon categorie;
	private int mise_avant_stand;
	private int mise_avant_evenement;
	private int participation_jeu;
	private TypeDeJeuSalon type_jeu;
	private String description1;
	private String description2;
	private int[] coordonnees = new int[2];
	private Cible cible_stand;
	private Cible cible_jeu;
	private Bitmap image;
	private Bitmap icone;
	
	/**
	 * Constructeur par défaut
	 */
	public Stand() {
		super();
	}
	/**
	 * Constructeur
	 * @param id_stand
	 * @param numero
	 * @param nom
	 * @param nom_representant
	 * @param prenom_representant
	 * @param type
	 * @param categorie
	 * @param mise_avant_stand
	 * @param mise_avant_evenement
	 * @param participation_jeu
	 * @param type_jeu
	 * @param description1
	 * @param description2
	 * @param coordonnees
	 * @param cible_stand
	 * @param cible_jeu
	 * @param image
	 * @param icone
	 */
	public Stand(int id_stand, String numero, String nom,
			String nom_representant, String prenom_representant,
			TypeDuStand type, CategorieDuSalon categorie,
			int mise_avant_stand, int mise_avant_evenement,
			int participation_jeu, TypeDeJeuSalon type_jeu,
			String description1, String description2, int[] coordonnees,
			Cible cible_stand, Cible cible_jeu, Bitmap image, Bitmap icone) {
		super();
		this.id_stand = id_stand;
		this.numero = numero;
		this.nom = nom;
		this.nom_representant = nom_representant;
		this.prenom_representant = prenom_representant;
		this.type = type;
		this.categorie = categorie;
		this.mise_avant_stand = mise_avant_stand;
		this.mise_avant_evenement = mise_avant_evenement;
		this.participation_jeu = participation_jeu;
		this.type_jeu = type_jeu;
		this.description1 = description1;
		this.description2 = description2;
		this.coordonnees = coordonnees;
		this.cible_stand = cible_stand;
		this.cible_jeu = cible_jeu;
		this.image = image;
		this.icone = icone;
	}
	/**
	 * @return id_stand
	 */
	public int getId_stand() {
		return id_stand;
	}
	/**
	 * @param id_stand
	 */
	public void setId_stand(int id_stand) {
		this.id_stand = id_stand;
	}
	/**
	 * @return numero
	 */
	public String getNumero() {
		return numero;
	}
	/**
	 * @param numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	/**
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return nom_representant
	 */
	public String getNom_representant() {
		return nom_representant;
	}
	/**
	 * @param nom_du_representant
	 */
	public void setNom_representant(String nom_du_representant) {
		this.nom_representant = nom_du_representant;
	}
	/**
	 * @return prenom_representant
	 */
	public String getPrenom_representant() {
		return prenom_representant;
	}
	/**
	 * @param prenom_du_representant
	 */
	public void setPrenom_representant(String prenom_du_representant) {
		this.prenom_representant = prenom_du_representant;
	}
	/**
	 * @return type
	 */
	public TypeDuStand getType() {
		return type;
	}
	/**
	 * @param type_du_stand
	 */
	public void setType(TypeDuStand type_du_stand) {
		this.type = type_du_stand;
	}
	/**
	 * @return categorie
	 */
	public CategorieDuSalon getCategorie() {
		return categorie;
	}
	/**
	 * @param categorie
	 */
	public void setCategorie(CategorieDuSalon categorie) {
		this.categorie = categorie;
	}
	/**
	 * @return mise_avant_stand
	 */
	public int getMise_avant_stand() {
		return mise_avant_stand;
	}
	/**
	 * @param mise_en_avant_de_stand
	 */
	public void setMise_avant_stand(int mise_en_avant_de_stand) {
		this.mise_avant_stand = mise_en_avant_de_stand;
	}
	/**
	 * @return mise_avant_evenement
	 */
	public int getMise_avant_evenement() {
		return mise_avant_evenement;
	}
	/**
	 * @param mise_en_avant_d_evenement
	 */
	public void setMise_avant_evenement(int mise_en_avant_d_evenement) {
		this.mise_avant_evenement = mise_en_avant_d_evenement;
	}
	/**
	 * @return participation_jeu
	 */
	public int getParticipation_jeu() {
		return participation_jeu;
	}
	/**
	 * @param participation_au_jeu
	 */
	public void setParticipation_jeu(int participation_au_jeu) {
		this.participation_jeu = participation_au_jeu;
	}
	/**
	 * @return type_jeu
	 */
	public TypeDeJeuSalon getType_jeu() {
		return type_jeu;
	}
	/**
	 * @param type_de_jeu
	 */
	public void setType_jeu(TypeDeJeuSalon type_de_jeu) {
		this.type_jeu = type_de_jeu;
	}
	/**
	 * @return description1
	 */
	public String getDescription1() {
		return description1;
	}
	/**
	 * @param description1
	 */
	public void setDescription1(String description1) {
		this.description1 = description1;
	}
	/**
	 * @return description2
	 */
	public String getDescription2() {
		return description2;
	}
	/**
	 * @param description2
	 */
	public void setDescription2(String description2) {
		this.description2 = description2;
	}
	/**
	 * @return coordonnees
	 */
	public int[] getCoordonnees() {
		return coordonnees;
	}
	/**
	 * @param coordonnees
	 */
	public void setCoordonnees(int[] coordonnees) {
		this.coordonnees = coordonnees;
	}
	/**
	 * @return cible_stand
	 */
	public Cible getCible_stand() {
		return cible_stand;
	}
	/**
	 * @param cible_stand
	 */
	public void setCible_stand(Cible cible_stand) {
		this.cible_stand = cible_stand;
	}
	/**
	 * @return cible_jeu
	 */
	public Cible getCible_jeu() {
		return cible_jeu;
	}
	/**
	 * @param cible_jeu
	 */
	public void setCible_jeu(Cible cible_jeu) {
		this.cible_jeu = cible_jeu;
	}
	/**
	 * @return image
	 */
	public Bitmap getImage() {
		return image;
	}
	/**
	 * @param image
	 */
	public void setImage(Bitmap image) {
		this.image = image;
	}
	/**
	 * @return icone
	 */
	public Bitmap getIcone() {
		return icone;
	}
	/**
	 * @param icone
	 */
	public void setIcone(Bitmap icone) {
		this.icone = icone;
	}
}
