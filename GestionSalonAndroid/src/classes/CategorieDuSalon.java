package classes;

/**
 * Enumeration CategorieDuSalon
 * @author ggosseli
 *
 */
public enum CategorieDuSalon {
MODE,
DECO,
HABITAT;
}
