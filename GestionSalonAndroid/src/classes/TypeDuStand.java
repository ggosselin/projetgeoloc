package classes;

/**
 * Enumeration TypeDuStand
 * @author ggosseli
 *
 */
public enum TypeDuStand {
SOCIETE,
ASSOCIATION,
ACCUEIL,
WC;
}
