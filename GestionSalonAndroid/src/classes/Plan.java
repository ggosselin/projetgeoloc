package classes;

public class Plan {
private int id_plan;
private String nom;
private EtatAvancement etat;

public Plan() {
	super();
	// TODO Auto-generated constructor stub
}

public Plan(int id, String nom, EtatAvancement etat) {
	super();
	this.id_plan = id;
	this.nom = nom;
	this.etat = etat;
}

public int getId_plan() {
	return id_plan;
}

public void setId_plan(int id_plan) {
	this.id_plan = id_plan;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

public EtatAvancement getEtat() {
	return etat;
}

public void setEtat(EtatAvancement etat) {
	this.etat = etat;
}

}
