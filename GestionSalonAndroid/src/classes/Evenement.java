package classes;

import java.sql.Timestamp;
import java.util.Date;
import android.graphics.Bitmap;


/**
 * Classe Evenement
 * @author ggosseli
 *
 */
public class Evenement {
	private int id_evenement;
	private String nom;
	private String nom_representant;
	private String prenom_representant;
	private Date date; 
	private Timestamp duree;
	private String description;
	private TypeDEvenement type;
	private int mise_avant_evenement;
	private Bitmap image;
	private Bitmap icone;
	private int id_salon_organisateur;
	private int id_stand_organisateur;
	private int id_stand_deroulement;

/**
 * Constructeur par défaut
 */
public Evenement() {
	super();
}

/**
 * Constructeur
 * @param id_evenement
 * @param nom
 * @param nom_representant
 * @param prenom_representant
 * @param date
 * @param duree
 * @param description
 * @param type
 * @param mise_avant_evenement
 * @param image
 * @param icone
 * @param id_salon_organisateur
 * @param id_stand_organisateur
 * @param id_stand_deroulement
 */
public Evenement(int id_evenement, String nom, String nom_representant,
		String prenom_representant, Date date, Timestamp duree,
		String description, TypeDEvenement type, int mise_avant_evenement,
		Bitmap image, Bitmap icone, int id_salon_organisateur, int id_stand_organisateur, 
		int id_stand_deroulement) {
	super();
	this.id_evenement = id_evenement;
	this.setNom(nom);
	this.setNom_representant(nom_representant);
	this.setPrenom_representant(prenom_representant);
	this.setDate(date);
	this.setDuree(duree);
	this.setDescription(description);
	this.setType(type);
	this.setMise_avant_evenement(mise_avant_evenement);
	this.setImage(image);
	this.setIcone(icone);
	this.id_salon_organisateur = id_salon_organisateur;
	this.id_stand_deroulement = id_stand_deroulement;
	this.id_stand_organisateur = id_stand_organisateur;
}

/**
 * @return id_evenement
 */
public int getId_evenement() {
	return id_evenement;
}

/**
 * @param id_evenement
 */
public void setId_evenement(int id_evenement) {
	this.id_evenement = id_evenement;
}

/**
 * @return nom
 */
public String getNom() {
	return nom;
}

/**
 * @param nom
 */
public void setNom(String nom) {
	this.nom = nom;
}

/**
 * @return nom_representant
 */
public String getNom_representant() {
	return nom_representant;
}

/**
 * @param nom_representant
 */
public void setNom_representant(String nom_representant) {
	this.nom_representant = nom_representant;
}

/**
 * @return prenom_representant
 */
public String getPrenom_representant() {
	return prenom_representant;
}

/**
 * @param prenom_representant
 */
public void setPrenom_representant(String prenom_representant) {
	this.prenom_representant = prenom_representant;
}

/**
 * @return date
 */
public Date getDate() {
	return date;
}

/**
 * @param date
 */
public void setDate(Date date) {
	this.date = date;
}

/**
 * @return duree
 */
public Timestamp getDuree() {
	return duree;
}

/**
 * @param duree
 */
public void setDuree(Timestamp duree) {
	this.duree = duree;
}

/**
 * @return description
 */
public String getDescription() {
	return description;
}

/**
 * @param description
 */
public void setDescription(String description) {
	this.description = description;
}

/**
 * @return type
 */
public TypeDEvenement getType() {
	return type;
}

/**
 * @param type
 */
public void setType(TypeDEvenement type) {
	this.type = type;
}

/**
 * @return mise_avant_evenement
 */
public int getMise_avant_evenement() {
	return mise_avant_evenement;
}

/**
 * @param mise_avant_evenement
 */
public void setMise_avant_evenement(int mise_avant_evenement) {
	this.mise_avant_evenement = mise_avant_evenement;
}

/**
 * @return image
 */
public Bitmap getImage() {
	return image;
}

/**
 * @param image
 */
public void setImage(Bitmap image) {
	this.image = image;
}

/**
 * @return icone
 */
public Bitmap getIcone() {
	return icone;
}

/**
 * @param icone
 */
public void setIcone(Bitmap icone) {
	this.icone = icone;
}

/**
 * @return id_salon_organisateur
 */
public int getId_salon_organisateur() {
	return id_salon_organisateur;
}

/**
 * @param id_salon_organisateur
 */
public void setId_salon_organisateur(int id_salon_organisateur) {
	this.id_salon_organisateur = id_salon_organisateur;
}

/**
 * @return id_stand_organisateur
 */
public int getId_stand_organisateur() {
	return id_stand_organisateur;
}

/**
 * @param id_stand_organisateur
 */
public void setId_stand_organisateur(int id_stand_organisateur) {
	this.id_stand_organisateur = id_stand_organisateur;
}

/**
 * @return id_stand_deroulement
 */
public int getId_stand_deroulement() {
	return id_stand_deroulement;
}

/**
 * @param id_stand_deroulement
 */
public void setId_stand_deroulement(int id_stand_deroulement) {
	this.id_stand_deroulement = id_stand_deroulement;
}

}
