package classes;

/**
 * Enumeration Cible
 * @author ggosseli
 *
 */
public enum Cible {
FAMILLE,
RETRAITE,
FILLE,
GARCON;
}