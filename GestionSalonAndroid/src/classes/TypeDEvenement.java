package classes;

/**
 * Enumeration TypeDEvenement
 * @author ggosseli
 *
 */
public enum TypeDEvenement {
Conférence,
Spectacle,
Dédicace;
}
