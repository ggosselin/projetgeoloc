package com.example.gestionsalonandroid;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import classes.Salon;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParseException;
import com.larvalabs.svgandroid.SVGParser;
import com.util.LireXML;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class AfficheSalonActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_affiche_salon);

		Bundle extra = getIntent().getExtras();
		final String nomSalon = extra.getString("nomSalon");
		Salon sTemp = new Salon();
		// On récupere le bon salon
		for (Salon s : LireXML.listeSalon) {
			if (s.getNom().equals(nomSalon))
				sTemp = s;
		}

		final Salon salon = sTemp;

		// create a new TableRow

		TextView tAdresse = (TextView) findViewById(R.id.textViewAdresse);
		TextView tCategorie = (TextView) findViewById(R.id.textViewCategorie);
		TextView tDescription = (TextView) findViewById(R.id.textViewDescription);
		TextView tDateDebut = (TextView) findViewById(R.id.textViewDateDebut);
		TextView tDateFin = (TextView) findViewById(R.id.textViewDateFin);
		TextView tRepresentant = (TextView) findViewById(R.id.textViewNomRepresentant);
		TextView tNomSalon = (TextView) findViewById(R.id.textViewNomSalon);
		ImageView ivImage = (ImageView) findViewById(R.id.imageViewImage);
		Button bouton = (Button) findViewById(R.id.buttonAffichePlan);
		ImageView ivIcone = (ImageView) findViewById(R.id.imageViewIcone);

		bouton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AfficheSalonActivity.this,
						AffichePlan.class);
				intent.putExtra("idSalon", salon.getId_salon());
				startActivity(intent);
			}
		});

		ivImage.setImageBitmap(salon.getImage());
		ivIcone.setImageBitmap(salon.getIcone());

		SimpleDateFormat month_date = new SimpleDateFormat("MMMMMMMMM",
				Locale.FRENCH);

		String month_dDeb = month_date.format(salon.getDate_debut().getTime());
		String month_dFin = month_date.format(salon.getDate_fin().getTime());

		tNomSalon.setText(nomSalon);
		tAdresse.setText(salon.getRue() + "\n" + salon.getCode_postal() + " "
				+ salon.getVille());
		tCategorie.setText(salon.getCategorie().toString());
		tRepresentant.setText(salon.getNom_representant() + " "
				+ salon.getPrenom_representant());
		tDescription.setText(salon.getDescription());
		tDateDebut.setText(salon.getDate_debut().get(Calendar.DATE) + " "
				+ month_dDeb + " " + salon.getDate_debut().get(Calendar.YEAR));
		tDateFin.setText(salon.getDate_fin().get(Calendar.DATE) + " "
				+ month_dFin + " " + salon.getDate_fin().get(Calendar.YEAR));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_salons_virtuels, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		

		// switch (item.getItemId()) {
		// case R.id.menu_refresh:

		// return true;
		if (item.getItemId() == R.id.menu_settings) {
			Intent intent1 = new Intent(AfficheSalonActivity.this,
					Settings.class);
			startActivity(intent1);
		}
		else 
		if (item.getItemId() == R.id.menu_refresh) {
			// Comportement du bouton "Rafraichir"
			finish();
			startActivity(getIntent());
		}
		// case R.id.menu_settings:
		// Comportement du bouton "Param�tres"
		// Intent intent1 = new Intent(AffichePlan.this, Settings.class);
		// startActivity(intent1);
		// return true;
		// default:
		
			return super.onOptionsItemSelected(item);
		}
	
}
