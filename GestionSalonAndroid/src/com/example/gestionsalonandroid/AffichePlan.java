package com.example.gestionsalonandroid;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import classes.Stand;
import classes.TouchImageView;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParseException;
import com.larvalabs.svgandroid.SVGParser;
import com.util.LireXML;
import com.util.UtilHTML;

public class AffichePlan extends Activity {

	EditText tRechercheStand = null;
	ImageButton iBoutonRecherche = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_affiche_plan);

		Bundle extras = getIntent().getExtras();
		int idSalon = extras.getInt("idSalon");
		InputStream is = UtilHTML
				.getDocumentHTML("http://stand-web-appli.olympe.in/dataExange.php?stands="
						+ idSalon);
		InputStream isPlan = UtilHTML
				.getDocumentHTML("http://stand-web-appli.olympe.in/dataExange.php?mapID="
						+ idSalon + "&png");

		Bitmap plan = BitmapFactory.decodeStream(isPlan);

		final List<Stand> listeStands = LireXML.lireStands(is);

		TouchImageView iv = (TouchImageView) findViewById(R.id.imageViewPlan);
		iv.setImageBitmap(plan);
		iv.setMaxZoom(4f);

		// Onclick sur l'image
		iv.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					Log.d("Coordonnees",
							"Touch coordinates : "
									+ String.valueOf(event.getX()) + "x"
									+ String.valueOf(event.getY()));
					int[] coord = new int[2];
					coord[0] = (int) event.getX();
					coord[1] = (int) event.getY();
					for (Stand st : listeStands) {
						System.out.println(st.getCoordonnees()[0] + " "
								+ st.getCoordonnees()[1]);
						if (st.getCoordonnees()[0] <= coord[0] + 14
								&& st.getCoordonnees()[0] >= coord[0] - 14
								&& st.getCoordonnees()[1] <= coord[1] + 14
								&& st.getCoordonnees()[1] >= coord[1] - 14) {
							chargerDonnees(st);
						}
					}
				}
				return false;
			}
		});

		// ajoutFleche(383,217);

		tRechercheStand = (EditText) findViewById(R.id.editTextRechercheStand);
		Log.d("PB", tRechercheStand.toString());

		// On récupère une ListView de notre layout en XML, c'est la vue qui
		// représente la liste
		ListView listeStands2 = (ListView) findViewById(R.id.listViewStand);

		/*
		 * On entrepose nos données dans un tableau qui contient deux colonnes
		 * : - la première contiendra le nom de l'utilisateur - la seconde
		 * contiendra le numéro de téléphone de l'utilisateur
		 */

		Bundle extras1 = getIntent().getExtras();
		String nomSalon = extras1.getString("nomSalon");

		/*
		 * Salon salonTemp = new Salon();
		 * 
		 * for (Salon s : LireXML.listeSalon) { if (s.getNom().equals(nomSalon))
		 * salonTemp = s; }
		 * 
		 * final Salon salon = salonTemp;
		 * 
		 * Stand standTemp = new Stand(); for (Stand s : salon.getListeStands())
		 * { String textSaisi = tRechercheStand.getText().toString(); if
		 * (s.getNom().equals(textSaisi)) standTemp = s; }
		 */

		final List<String> liste = new ArrayList<String>();
		;

		// Pour chaque personne dans notre répertoire…
		for (int i = 0; i < listeStands.size(); i++) {
			liste.add(listeStands.get(i).getNom());
		}

		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, liste);

		// Pour finir, on donne à la ListView le SimpleAdapter
		listeStands2.setAdapter(adapter);

		// Listeners
		listeStands2.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(),
						"OnItemClickListener item recherche !",
						Toast.LENGTH_SHORT).show();
				System.out.println("OnItemClickListener item recherche !");
			}
		});

		tRechercheStand.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				System.out.println("onTextChanged recherche !");
				adapter.getFilter().filter(s);
			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}
		});

		iBoutonRecherche = (ImageButton) findViewById(R.id.imageButtonRecherche);

		iBoutonRecherche.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(),
						"OnClickListener bouton recherche !",
						Toast.LENGTH_SHORT).show();
				/*
				 * for(int i = 0; i>liste.size();i++){ String textSaisi =
				 * tRechercheStand.getText().toString();
				 * if(textSaisi==liste.get(i)){
				 * 
				 * } }
				 */
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_affiche_plan, menu);
		return true;
	}

	public void chargerDonnees(Stand stand) {
		AlertDialog.Builder adb = new AlertDialog.Builder(this);

		// get your custom_toast.xml ayout
		LayoutInflater inflater = getLayoutInflater();

		View layout = inflater.inflate(R.layout.toast_infos_stand,
				(ViewGroup) findViewById(R.id.linearLayoutInfosStand));

		TextView tvNumStand = (TextView) layout
				.findViewById(R.id.textViewNumStand);
		TextView tvNomStand = (TextView) layout
				.findViewById(R.id.textViewNomStand);
		TextView tvRepresentant = (TextView) layout
				.findViewById(R.id.textViewNomRepresentant);
		TextView tvType = (TextView) layout.findViewById(R.id.textViewType);
		TextView tvCategorie = (TextView) layout
				.findViewById(R.id.textViewCategorie);
		TextView tvDescription = (TextView) layout
				.findViewById(R.id.textViewDescription);
		ImageView ivImage = (ImageView) layout
				.findViewById(R.id.imageViewImageStand);
		ImageView ivIcone = (ImageView) layout
				.findViewById(R.id.imageViewIconeStand);

		tvNumStand.setText(stand.getNumero());
		tvNomStand.setText(stand.getNom());
		tvRepresentant.setText(stand.getNom_representant() + " "
				+ stand.getPrenom_representant());
		tvType.setText(stand.getType().name());
		tvCategorie.setText(stand.getCategorie().name());
		tvDescription.setText(stand.getDescription2());
		System.out.println(ivImage);
		System.out.println(ivIcone);
		ivImage.setImageBitmap(stand.getImage());
		ivIcone.setImageBitmap(stand.getIcone());

		// On affecte la vue personnalisé que l'on a crée à notre AlertDialog
		adb.setView(layout);

		// On affecte un bouton "OK" à notre AlertDialog et on lui affecte un
		// évènement
		adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		adb.show();
	}

	public void ajoutFleche(int x, int y) {
		// Ajout fleche
		ImageView ivFleche = (ImageView) findViewById(R.id.imageFleche);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				50, 40);
		params.setMargins(x - 30, y - 66, 0, 0); // x-32 , y-67

		SVG svgFleche = null;
		try {
			svgFleche = SVGParser.getSVGFromAsset(getAssets(), "flecheBas.svg");
			ivFleche.setImageDrawable(svgFleche.createPictureDrawable());
			ivFleche.setLayoutParams(params);

		} catch (SVGParseException e) {
			Log.e("SVGParseException", e.getMessage());
		} catch (IOException e) {
			Log.e("Deb", e.getMessage());
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_settings) {
			Intent intent1 = new Intent(AffichePlan.this, Settings.class);
			startActivity(intent1);
		} else if (item.getItemId() == R.id.menu_refresh) {
			// Comportement du bouton "Rafraichir"
			finish();
			startActivity(getIntent());
		}
		return super.onOptionsItemSelected(item);
	}
}
