package com.example.gestionsalonandroid;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import classes.Salon;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParseException;
import com.larvalabs.svgandroid.SVGParser;
import com.util.LireXML;
import com.util.UtilHTML;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.Path;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AlphabetIndexer;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SectionIndexer;

public class SalonsVirtuels extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salons_virtuels);
             
        EditText et = (EditText)findViewById(R.id.editTextRechercheSalon);
        
        ListView listeSalons = (ListView)findViewById(R.id.listeSalons);
        List<String> salons = new ArrayList<String>();
        
        InputStream is = UtilHTML.getDocumentHTML("http://stand-web-appli.olympe.in/dataExange.php?salons");
        
        try {
			LireXML.lireSalon(is);
			for(Salon s : LireXML.listeSalon)
			{
				salons.add(s.getNom());
			}
		} catch (ParserConfigurationException e) {
			Log.e("SalonsVirtuels ParserConfigurationException", e.getMessage());
		} catch (SAXException e) {
			Log.e("SalonsVirtuels SAXException", e.getMessage());
		} catch (IOException e) {
			Log.e("SalonsVirtuels IOException", e.getMessage());
		}
        
        //Adapteur pour ListView
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
		  android.R.layout.simple_list_item_1, android.R.id.text1, salons);
		
		// Assign adapter to ListView
		listeSalons.setAdapter(adapter);
		
        //Listeners
        listeSalons.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				chargerInfos(arg0.getItemAtPosition(arg2).toString());
			}
        });
        
        et.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				adapter.getFilter().filter(s);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_salons_virtuels, menu);
        return true;
    }
    
    public void chargerInfos(String nomItem)
    {
    	Intent intent = new Intent(SalonsVirtuels.this, AfficheSalonActivity.class);
    	intent.putExtra("nomSalon", nomItem);
    	startActivity(intent);
    }
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	if (item.getItemId() == R.id.menu_settings) {
			Intent intent1 = new Intent(SalonsVirtuels.this,
					Settings.class);
			startActivity(intent1);
		}
		else 
		if (item.getItemId() == R.id.menu_refresh) {
			// Comportement du bouton "Rafraichir"
			finish();
			startActivity(getIntent());
		}
			return super.onOptionsItemSelected(item);
		}
	}

