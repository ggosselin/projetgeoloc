package com.example.gestionsalonandroid;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import classes.Parametres;

public class Settings extends Activity {
	OnClickListener checkBoxListener;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		final CheckBox chVibrate = (CheckBox) findViewById(R.id.chVivrate);
		final CheckBox chSound = (CheckBox) findViewById(R.id.chSound);
		final CheckBox chNotif = (CheckBox) findViewById(R.id.chNotif);
		
		chVibrate.setChecked(Parametres.vibreurActif);
		chVibrate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// is chVibrate checked?
				if (!chVibrate.isChecked()) {
					Parametres.vibreurActif = false;
					Log.d("vibreur des", "okk");
				}
				else
				{
					Parametres.vibreurActif = true;
				}				
			}
		});		
		
	

	chSound.setChecked(Parametres.sonActif);
	chSound.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// is chVibrate checked?
			if (!chSound.isChecked()) {
				Parametres.sonActif = false;
			}
			else
			{
				Parametres.sonActif = true;
			}				
		}
	});		
	
	chNotif.setChecked(Parametres.notifActif);
	chNotif.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// is chVibrate checked?
			if (!chNotif.isChecked()) {
				Parametres.notifActif = false;
			}
			else
			{
				Parametres.notifActif = true;
			}				
		}
	});		
}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_settings, menu);
		return true;
	}

}
