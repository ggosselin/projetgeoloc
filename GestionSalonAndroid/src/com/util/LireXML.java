package com.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import classes.CategorieDuSalon;
import classes.Cible;
import classes.Evenement;
import classes.Salon;
import classes.Stand;
import classes.TypeDEvenement;
import classes.TypeDeJeuSalon;
import classes.TypeDuStand;

/**
 * Classe permettant de lire un fichier XML de Salons, Stands ou Evenement à partir d'un InputStream
 * @author ggosseli
 *
 */
public class LireXML {
	public static List<Salon> listeSalon;
	public static List<Stand> listeStands;
	public static Evenement evenement;
	
	/**
	 * Méthode permettant de lire un fichier XML contenant les salons
	 * @param file le fichier à lire
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static void lireSalon(InputStream file) throws ParserConfigurationException, SAXException, IOException
	{
		listeSalon = new ArrayList<Salon>();
		
		Document doc = getDoc(file);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		NodeList nSalons = doc.getElementsByTagName("salon");
		
		for(int i=0;i<nSalons.getLength();i++)
		{
			Node nSalon = nSalons.item(i);
			
			if(nSalon.getNodeType()== Node.ELEMENT_NODE)
			{
				Element e = (Element)nSalon;
				CategorieDuSalon categ= CategorieDuSalon.valueOf(e.getElementsByTagName("categorie").item(0).getTextContent());
				Calendar dDeb = Calendar.getInstance();
				Calendar dFin = Calendar.getInstance();
				Bitmap image = BitmapFactory.decodeStream(UtilHTML.getDocumentHTML("http://stand-web-appli.olympe.in"+e.getElementsByTagName("image").item(0).getTextContent()));
				Bitmap icone = BitmapFactory.decodeStream(UtilHTML.getDocumentHTML("http://stand-web-appli.olympe.in"+e.getElementsByTagName("icone").item(0).getTextContent()));
				Date deb = new Date();
				Date fin = new Date();
				
				try {
					deb = formatter.parse(e.getElementsByTagName("date_debut").item(0).getTextContent());
					fin = formatter.parse(e.getElementsByTagName("date_fin").item(0).getTextContent());
				} catch (DOMException e1) {
					Log.e("LireXML DOMException", e1.getMessage());
				} catch (ParseException e1) {
					Log.e("LireXML ParseException", e1.getMessage());
				}
				dDeb.setTime(deb);
				dFin.setTime(fin);
				TypeDeJeuSalon typeJeuSalon = TypeDeJeuSalon.valueOf(e.getElementsByTagName("type_jeu").item(0).getTextContent());
				
				Salon salon = new Salon(Integer.parseInt(e.getElementsByTagName("id_salon").item(0).getTextContent()), 
						e.getElementsByTagName("nom").item(0).getTextContent(), e.getElementsByTagName("nom_representant").item(0).getTextContent(), 
						e.getElementsByTagName("prenom_representant").item(0).getTextContent(), categ, dDeb, dFin, 
						e.getElementsByTagName("rue").item(0).getTextContent(),Integer.parseInt(e.getElementsByTagName("code_postal").item(0).getTextContent()), 
						e.getElementsByTagName("ville").item(0).getTextContent(),e.getElementsByTagName("description").item(0).getTextContent(), 
						Integer.parseInt(e.getElementsByTagName("mise_avant_stand").item(0).getTextContent()), Integer.parseInt(e.getElementsByTagName("mise_avant_evenement").item(0).getTextContent()),	
						Integer.parseInt(e.getElementsByTagName("participation_jeu").item(0).getTextContent()), 
						image, icone, typeJeuSalon, Integer.parseInt(e.getElementsByTagName("etat").item(0).getTextContent()), 
						Integer.parseInt(e.getElementsByTagName("version").item(0).getTextContent()));
				
				listeSalon.add(salon);
			}
		}
	}
	
	/**
	 * Méthode permettant de lire un fichier XML contenant les salons
	 * @param file le fichier des stands
	 * @return listeStand la liste des stands associés au salon
	 */
	public static List<Stand> lireStands(InputStream file)
	{
		System.out.println("STAND OK");
		Document doc = null;
		try {
			doc = getDoc(file);
		} catch (SAXException e) {
			Log.e("LireStand SAXException", e.getMessage());
		} catch (IOException e) {
			Log.e("LireStand IOException", e.getMessage());
		} catch (ParserConfigurationException e) {
			Log.e("LireStand ParserConfigurationException", e.getMessage());
		}
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		
		NodeList nStands = doc.getElementsByTagName("stand");
		
		List<Stand> listeStand = new ArrayList<Stand>();
		
		for(int j=0;j<nStands.getLength();j++)
		{
			Node nStand = nStands.item(j);
			
			if(nStand.getNodeType()== Node.ELEMENT_NODE)
			{
				Element el = (Element)nStand;
				TypeDuStand typeStand= TypeDuStand.valueOf(el.getElementsByTagName("type").item(0).getTextContent());
				CategorieDuSalon categSalon = CategorieDuSalon.valueOf(el.getElementsByTagName("categorie").item(0).getTextContent());
				TypeDeJeuSalon typeJeu = TypeDeJeuSalon.valueOf(el.getElementsByTagName("type_jeu").item(0).getTextContent());
				Cible cibleStand = Cible.valueOf(el.getElementsByTagName("cible_stand").item(0).getTextContent());
				Cible cibleJeu = Cible.valueOf(el.getElementsByTagName("cible_jeu").item(0).getTextContent());
				Bitmap imageStand = BitmapFactory.decodeStream(UtilHTML.getDocumentHTML("http://stand-web-appli.olympe.in"+el.getElementsByTagName("image").item(0).getTextContent()));
				Bitmap iconeStand = BitmapFactory.decodeStream(UtilHTML.getDocumentHTML("http://stand-web-appli.olympe.in"+el.getElementsByTagName("icone").item(0).getTextContent()));
				
				int[] coord = new int[2];
				coord[0] = Integer.parseInt(el.getElementsByTagName("coordonnees_x").item(0).getTextContent());
				coord[1] = Integer.parseInt(el.getElementsByTagName("coordonnees_y").item(0).getTextContent());
				
				Stand stand = new Stand(Integer.parseInt(el.getElementsByTagName("id_stand").item(0).getTextContent()), 
						el.getElementsByTagName("numero").item(0).getTextContent(), el.getElementsByTagName("nom").item(0).getTextContent(),
						el.getElementsByTagName("nom_representant").item(0).getTextContent(), el.getElementsByTagName("prenom_representant").item(0).getTextContent(), 
						typeStand, categSalon, Integer.parseInt(el.getElementsByTagName("mise_avant_stand").item(0).getTextContent()), 
						Integer.parseInt(el.getElementsByTagName("mise_avant_stand").item(0).getTextContent()),	Integer.parseInt(el.getElementsByTagName("participation_jeu").item(0).getTextContent()), 
						typeJeu, el.getElementsByTagName("description1").item(0).getTextContent(), el.getElementsByTagName("description2").item(0).getTextContent(), 
						coord,cibleStand,cibleJeu,imageStand,iconeStand);
				listeStand.add(stand);
			}
		}
		return listeStand;
	}
	
	/**
	 * Méthode permettant de lire un fichier XML contenant les evenements
	 * @param file le fichier contenant les evenements
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static void lireEvenement(InputStream file) throws SAXException, IOException, ParserConfigurationException
	{
		Document doc = getDoc(file);
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		
		NodeList nEvenements = doc.getElementsByTagName("Evenement");
		
		Node nEvenement = nEvenements.item(0);
			
		if(nEvenement.getNodeType()== Node.ELEMENT_NODE)
		{
			Element e = (Element)nEvenement;
			Date date = new Date();
			try {
				date = formatter.parse(e.getElementsByTagName("date").item(0).getTextContent());
			} catch (DOMException e1) {
				Log.e("LireXML DOMException", e1.getMessage());
			} catch (ParseException e1) {
				Log.e("LireXML ParseException", e1.getMessage());
			}
			Timestamp duree = new Timestamp(Integer.parseInt(e.getElementsByTagName("duree").item(0).getTextContent()));
			TypeDEvenement type = TypeDEvenement.valueOf(e.getElementsByTagName("type").item(0).getTextContent());
				
			evenement = new Evenement(Integer.parseInt(e.getElementsByTagName("id_evenement").item(0).getTextContent()), 
					e.getElementsByTagName("nom").item(0).getTextContent(), e.getElementsByTagName("nom_representant").item(0).getTextContent(), 
					e.getElementsByTagName("prenom_representant").item(0).getTextContent(), date,duree, 
					e.getElementsByTagName("description").item(0).getTextContent(),type,
					Integer.parseInt(e.getElementsByTagName("mise_avant_evenement").item(0).getTextContent()),
					null,null,
					Integer.parseInt(e.getElementsByTagName("id_salon_organisateur").item(0).getTextContent()),
					Integer.parseInt(e.getElementsByTagName("id_stand_organisateur").item(0).getTextContent()),
					Integer.parseInt(e.getElementsByTagName("id_stand_deroulement").item(0).getTextContent()));
		}
	}
	
	public static Document getDoc(InputStream file) throws SAXException, IOException, ParserConfigurationException
	{
		DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		// report Errors if DTD validation is On
		dBuilder.setErrorHandler(new ErrorHandler(){

			public void error(SAXParseException arg0) throws SAXException {
				Log.e("error", arg0.getMessage());
			}

			public void fatalError(SAXParseException arg0) throws SAXException {
				Log.e("fatalError", arg0.getMessage());
			}

			public void warning(SAXParseException exception)
					throws SAXException {
				Log.e("warning", exception.getMessage());
			}
			
		});
		Document doc = dBuilder.parse(file);
		doc.getDocumentElement().normalize();
		return doc;
	}
}