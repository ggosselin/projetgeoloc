package com.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.StrictMode;
import android.util.Log;

/**
 * Classe permettant de récuperer un fichier à partir de son URL
 * @author ggosseli
 *
 */
public class UtilHTML {
	
	/**
	 * @param adresse l'adresse URL du fichier
	 * @return is InputStream 
	 */
	public static InputStream getDocumentHTML(String adresse)
	{
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
		
		InputStream is = null;
		
		try
		{
			HttpClient client = new DefaultHttpClient();
	        HttpGet request = new HttpGet();
	        request.setURI(new URI(adresse.replace(" ", "%20")));
	        HttpResponse response = client.execute(request);
	        is = response.getEntity().getContent();
	        
        } catch (IOException e) {
			Log.e("UtilHTML IOException", e.getMessage());
		} catch (URISyntaxException e) {
			Log.e("UtilHTML URISyntaxException", e.getMessage());
		}
		
		return is;
	}

}
