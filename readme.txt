Création du dossier :

$ mkdir /path/to/your/project
$ cd /path/to/your/project
$ git init
$ git clone https://<username>@bitbucket.org/ggosselin/ProjetGeoloc.git

Commit :

$ git add <file>
$ git commit -m "<message>"
$ git push

Mettre à jour son dossier :

$ git pull
