package com.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import android.util.Log;

public class UtilXML {
	public static List<Etudiant> listeEtudiant;
	
	public static void lire(InputStream file) throws ParserConfigurationException, SAXException, IOException
	{
		listeEtudiant = new ArrayList<Etudiant>();
		/*DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		
		// use DTD validation
		dbFactory.setNamespaceAware(true);
		dbFactory.setValidating(true);*/
		DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		// report Errors if DTD validation is On
		// note: ErrorHandler must be defined
		dBuilder.setErrorHandler(new ErrorHandler(){

			public void error(SAXParseException arg0) throws SAXException {
				// TODO Auto-generated method stub
				System.out.println("Erreur");
			}

			public void fatalError(SAXParseException arg0) throws SAXException {
				// TODO Auto-generated method stub
				System.out.println("Erreur");
			}

			public void warning(SAXParseException exception)
					throws SAXException {
				// TODO Auto-generated method stub
				System.out.println("Erreur");
			}
			
		});
		Document doc = dBuilder.parse(file);
		doc.getDocumentElement().normalize();
		
		NodeList nPersonnes = doc.getElementsByTagName("Etudiant");
		
		for(int i=0;i<nPersonnes.getLength();i++)
		{
			Node nPersonne = nPersonnes.item(i);
			if(nPersonne.getNodeType()== Node.ELEMENT_NODE)
			{
				Element e = (Element)nPersonne;
				Etudiant etu = new Etudiant(Integer.parseInt(e.getElementsByTagName("id").item(0).getTextContent()),
						e.getElementsByTagName("nom").item(0).getTextContent(),e.getElementsByTagName("prenom").item(0).getTextContent());
				listeEtudiant.add(etu);
			}
			
		}
	}
}
