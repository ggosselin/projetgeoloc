package com.example.androidxml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.util.Etudiant;
import com.util.UtilXML;

import android.os.Bundle;
import android.app.Activity;
import android.content.res.XmlResourceParser;
import android.util.Log;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView t = new TextView(this);
		try {
			InputStream xmlFile = getAssets().open("Etudiant.xml");
			UtilXML.lire(xmlFile);
			for(Etudiant e : UtilXML.listeEtudiant)
			{
				t.append("Etudiant "+e.getId()+" : "+e.getNom()+" "+e.getPrenom()+"\n");
			}
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			t.setText(e1.getMessage());
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			t.setText(e.getMessage());
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			t.setText(e.getMessage());
		}
		setContentView(t);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}
